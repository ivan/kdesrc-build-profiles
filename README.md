# kdesrc-build-profiles

Build profiles for `kdesrc-build` -- a tool that allows defining build
profiles on top of the `kdesrc-build` system for building KDE packages.
